<x-layout>
    <header class="container">
        <div class="row text-center">
            <h1 class="m-2">Dettaglio Cliente</h1>
        </div>
    </header>

    @if (session('flash'))
    <div class="toast-custom">
      <div class="alert alert-success m-0 d-flex align-items-center">
        <i class="fa-solid fa-check"></i><span class="ms-2">{{ session('flash')}}</span>
      </div>
      <div class="toast-line-bg-success">
        <div class="toast-line-success"></div>
      </div>
    </div>
    @endif

    <section class="container">
      <div class="row">

        <div class="col-12 col-md-6">
          <div class="card">
            <p class="card-title m-3 display-4">{{$customer->name}} {{$customer->surname}}</p>
            <p class="card-subtitle m-2 display-6">{{$customer->company}}</p>
            <div class="accordion accordion-flush" id="accordionExample">
              <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <h5>Annotazioni:</h5>
                  </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse " aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                    <p class="card-text m-3">{{$customer->body}}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer d-flex justify-content-between mt-3">
              <a href="tel:{{$customer->contact}}">{{$customer->contact}}</a>
              <a href="mailto:{{$customer->email}}">{{$customer->email}}</a>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-6">
          <img src="{{Storage::url($customer->image)}}" alt="foto cliente" class="img-fluid">
        </div>

        <div class="col-12 d-inline justify-content-around">
          <a href="{{route('customer.edit', compact('customer'))}}" class="btn btn-warning m-2">Modifica Cliente</a>
          <form id="form-destroy" action="{{route('customer.destroy', compact('customer'))}}" method="post" style="display: inline; ">
            @csrf
            @method('delete')
            <button type="submit" class="btn btn-danger">Elimina Cliente</button>
          </form>
        </div>
        
      </div>
    </section>
</x-layout>

    {{-- <section class="container">

        BISOGNA CREARE UNA NUOVA TABELLA PER CONTENERE TUTTE LE IMMAGINI CARRICATE PER OGNI CLIENTE, ED UTILIZZARE UN FOREACH CON UN CAROSELLO PER AVERE
        UNA VISUALIZZAZIONE RAPIDA NELLA PAGINA DETTAGLIO DEL CLIENTE.

        <picture class="row w-100">
            <div id="carouselExampleIndicators" class="carousel slide col-12" data-bs-ride="carousel" style="max-width: 600px;">
                <div class="carousel-indicators">
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                  <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner m-5" >
                  <div class="carousel-item active">
                    <img src="{{Storage::url($customer->image)}}" class="d-block w-100 img-fluid" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="{{Storage::url($customer->image)}}" class="d-block w-100" alt="...">
                  </div>
                  <div class="carousel-item">
                    <img src="{{Storage::url($customer->image)}}" class="d-block w-100" alt="...">
                  </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
        </picture>
    </section> --}}