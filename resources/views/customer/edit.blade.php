<x-layout>

    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                <h1>Modifica CLIENTE</h1>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container mt-4">
        <div class="row ">

            <form class="row border" action="{{route('customer.update', compact('customer'))}}" method="POST" enctype="multipart/form-data">
            
                @csrf

                @method('put')

                <div class="mb-3 mt-3 col-12 col-md-6">
                    <label for="exampleInputName" class="form-label">Nome:</label>
                    <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="exampleInputName" aria-describedby="Name" value="{{$customer->name}}">
                </div>

                <div class="mb-3 mt-3 col-12 col-md-6">
                    <label for="exampleInputSurname" class="form-label">Cognome:</label>
                    <input name="surname" type="text" class="form-control @error('surname') is-invalid @enderror" id="exampleInputSurname" aria-describedby="Surname" value="{{$customer->surname}}">
                </div>

                <div class="mb-3 col-12 col-md-6">
                    <label for="exampleInputEmail" class="form-label">Email:</label>
                    <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail" aria-describedby="email" value="{{$customer->email}}">
                </div>

                <div class="mb-3 col-12 col-md-6">
                    <label for="exampleInputContact" class="form-label">Contacts:</label>
                    <input name="contact" type="tel" class="form-control @error('contact') is-invalid @enderror" id="exampleInputContact" aria-describedby="contact" value="{{$customer->contact}}">
                </div>

                <div class="mb-3 col-12 col-md-6">
                    <label for="exampleInputCompany" class="form-label">Company:</label>
                    <input name="company" type="text" class="form-control @error('company') is-invalid @enderror" id="exampleInputCompany" aria-describedby="company" value="{{$customer->company}}">
                </div>

                <div class="mb-3 col-12">
                    <label for="exampleInputBody" class="form-label">Annotazioni:</label>
                    <textarea name="body" id="exampleInputBody" class="form-control @error('body') is-invalid @enderror" cols="20" rows="10">{{$customer->body}}</textarea>
                </div>

                <div class="mb-3">
                    <label for="exampleInputImage" class="form-label">Immagine:</label>
                    <input name="image" type="file" class="form-control @error('image') is-invalid @enderror" id="exampleInputImage" aria-describedby="Image" accept="image/jpeg, image/png" value="{{old('image')}}">
                </div>

                <button type="submit" class="btn btn-primary mb-3">Modifica Cliente</button>

            </form>
        </div>
    </div>
</x-layout>