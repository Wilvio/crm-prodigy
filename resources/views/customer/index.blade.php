<x-layout>

    <header class="container">
        <div class="row">
            <div class="col-12">
                <h1>Pagina clienti</h1>
            </div>
        </div>
    </header>

    @if (session('flash'))
    <div class="toast-custom">
      <div class="alert alert-success m-0 d-flex align-items-center">
        <i class="fa-solid fa-check"></i><span class="ms-2">{{ session('flash')}}</span>
      </div>
      <div class="toast-line-bg-success">
        <div class="toast-line-success"></div>
      </div>
    </div>
    @endif

    @if (count($customers) == 0)
        <div class="container my-5 text-center">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                    <p class="display-3">Non sono ancora stati aggiunti clienti.</p>
                </div>
            </div>
        </div>
    @else
        <section class="container-fluid m-4">
            <div class="row d-flex flex-column">
                @foreach ($customers as $customer)
                    <x-customer :customer="$customer" :loopindex="$loop->index"/>
                @endforeach
            </div>
        </section>
    @endif
</x-layout>