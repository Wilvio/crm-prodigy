<x-layout>

    <div class="container m-5">
        <div class="row">
            <div class="col-12">
                <h1>Modifica PREVENTIVO</h1>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container mt-4">
        <div class="row ">

            <form class="row border" action="{{route('estimate.update', compact('estimate'))}}" method="POST" enctype="multipart/form-data">
            
                @csrf

                @method('put')

                <div class="mb-3 mt-3 col-12 col-md-6">
                    <label for="exampleInputProductName" class="form-label">Nome Prodotto:</label>
                    <input name="product_name" type="text" class="form-control @error('product_name') is-invalid @enderror" id="exampleInputProductName" aria-describedby="product name" value="{{$estimate->product_name}}" required>
                </div>

                <div class="mb-3 mt-3 col-12 col-md-6">
                    <label for="exampleInputPrice" class="form-label">Prezzo proposto:</label>
                    <input name="price" type='currency' class="form-control @error('price') is-invalid @enderror" id="exampleInputPrice" aria-describedby="price" value="{{$estimate->price}}" required>
                </div>

                <div class="mb-3 col-12">
                    <label for="exampleInputDescription" class="form-label">Descrizione:</label>
                    <textarea name="description" id="exampleInputDescription" class="form-control @error('description') is-invalid @enderror" cols="30" rows="10" required>{{$estimate->description}}</textarea>
                </div>

                <button type="submit" class="btn btn-primary mb-3">Modifica Preventivo</button>

            </form>
        </div>
    </div>
</x-layout>