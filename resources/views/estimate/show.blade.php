<x-layout>
    <header class="container">
        <div class="row text-center">
            <h1 class="m-2">Dettagli</h1>
        </div>
    </header>

    @if (session('flash'))
    <div class="toast-custom">
      <div class="alert alert-success m-0 d-flex align-items-center">
        <i class="fa-solid fa-check"></i><span class="ms-2">{{ session('flash')}}</span>
      </div>
      <div class="toast-line-bg-success">
        <div class="toast-line-success"></div>
      </div>
    </div>
    @endif

    <section class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-title m-3 display-4">{{$estimate->product_name}}</div>
                    <div class="card-subtitle m-2 display-6">€ {{$estimate->price}}</div>
                    
                    <div class="accordion accordion-flush" id="accordionExample">
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              <h5>Annotazioni:</h5>
                            </button>
                          </h2>
                          <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <p class="card-text m-3">{{$estimate->description}}</p>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="card-footer">Cliente:
                      <a href="{{route('customer.show', $estimate->id_customer)}}">{{$estimate->customer->name}} {{$estimate->customer->surname}}</a>
                    </div>
                </div>
            </div>

            @if ($estimate->is_accepted === null)
              <div class="col-12 d-inline justify-content-around">
                <form action="{{route('estimate.accepted', $estimate->id)}}" method="POST" style="display: inline;">
                  @csrf
                  <button class="btn btn-success" type="submit">Crea Offerta</button>
                </form>
                
                <form action="{{route('estimate.declined', $estimate->id)}}" method="POST" style="display: inline;">
                  @csrf
                  <button class="btn btn-warning" type="submit">Rifiuta Preventivo</button>
                </form>
                
                <a href="{{route('estimate.edit', compact('estimate'))}}" class="btn btn-info m-2">Modifica Preventivo</a>
                
                <form id="form-destroy" action="{{route('estimate.destroy', compact('estimate'))}}" method="post" style="display: inline;">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger">Elimina Preventivo</button>
                </form>
              </div>
            @else
              <div class="col-12 d-inline justify-content-around">
                <form action="{{route('estimate.undo', $estimate->id)}}" method="POST" style="display: inline;">
                  @csrf
                  <button class="btn btn-success" type="submit">Annulla</button>
                </form>

                <form id="form-destroy" action="{{route('estimate.destroy', compact('estimate'))}}" method="post" style="display: inline;">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger">Elimina</button>
                </form>
              </div>
            @endif
        </div>
    </section>

</x-layout>