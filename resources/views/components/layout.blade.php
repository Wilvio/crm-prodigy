<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- css --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">


    <title>CRM Prodigy</title>

</head>
  <body class="bg-custom2">
      <x-navbar/>


    {{$slot}}


    {{-- Js --}}
    <script src="{{asset('js/app.js')}}"></script>

  </body>
</html>