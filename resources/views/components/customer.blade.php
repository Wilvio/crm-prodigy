
<div class="card m-3" style="max-width: 540px; min-width: 540px; min-height: 100px; max-height: 300px;">
    <div class="row g-0">
      <div class="col-md-4">
        <img src="{{Storage::url($customer->image)}}" class="img-fluid rounded-start" alt="immagine inserita nel form">
      </div>
      <div class="col-md-8">
            <div class="card-body">
                <h4 class="card-title">{{$customer->name}} {{$customer->surname}}</h4>
                <h5 class="card-text">{{$customer->email}}</h5>
                <h5 class="card-text">{{$customer->contact}}</h5>
                <p class="card-text"><small class="text-muted">{{$customer->updated_at}}</small></p>
                <a href="{{route('customer.show', compact('customer'))}}" class="btn btn-primary">Dettagli</a>
            </div>
      </div>
    </div>
  </div>