<div class="card m-3" style="max-width: 540px; min-width: 540px; min-height: 100px; max-height: 300px;">
    <div class="row g-0">
      <div class="col-md-8">
        <div class="card-body">
          <h4 class="card-title">{{$estimate->product_name}}</h4>
          <h5 class="card-text">€{{$estimate->price}}</h5>
          <a href="{{route('customer.show', $estimate->id_customer)}}" class="card-link">{{$estimate->customer->name}}{{$estimate->customer->surname}}</a>
          <p class="card-text"><small class="text-muted">{{$estimate->updated_at->format('d/n/Y')}}</small></p>
          <a href="{{route('estimate.show', compact('estimate'))}}" class="btn btn-primary">Dettagli</a>
        </div>
      </div>
    </div>
  </div>