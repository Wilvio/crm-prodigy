<x-layout>
    <header class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>C.R.M.</h1>
            </div>
            <div class="col-12 col-md-6">
                <h2>Customer Relationship Management</h2>
            </div>
        </div>
    </header>

    <section class="container m-5">
        <div class="row">
            <div class="col-12 col-md-6">
                @if (count($customer) == 0)
                    <div class="row justify-content-center align-items-center">
                        <div class="col-12 col-md-6">
                            <p class="display-3">Non sono ancora stati aggiunti clienti.</p>
                        </div>
                    </div>
                @else
                    <div class="row d-flex flex-column">
                        @foreach ($customer as $customer)
                            <x-customer :customer="$customer" :loopindex="$loop->index"/>
                        @endforeach
                    </div>
                @endif
            </div>

            <div class="col-12 col-md-6">
                @if (count($estimate) == 0)
                    <div class="row justify-content-center align-items-center">
                        <div class="col-12 col-md-6">
                            <p class="display-3 text-center">Non ci sono OFFERTE Attive.</p>
                        </div>
                    </div>
                @else
                    <section class="container-fluid m-4">
                        <div class="row d-flex flex-column">
                            @foreach ($estimate as $estimate)
                                <div class="card m-3" style="max-width: 540px; min-width: 340px; min-height: 100px; max-height: 300px;">
                                    <div class="row g-0">
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h4 class="card-title">{{$estimate->product_name}}</h4>
                                                <h5 class="card-text">€ {{$estimate->price}}</h5>
                                                <a href="{{route('customer.show', $estimate->id_customer)}}" class="card-text">{{$estimate->customer->name}} {{$estimate->customer->surname}}</a>
                                                <p class="card-text"><small class="text-muted">{{$estimate->updated_at->format('d/n/Y')}}</small></p>
                                                <a href="{{route('estimate.show', compact('estimate'))}}" class="btn btn-primary">Dettagli</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </section>
                @endif
            </div>
        </div>
    </section>
</x-layout>