<?php

namespace App\Models;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Estimate extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_name',
        'description',
        'price',
        'id_customer',
    ];

    static public function inProgress()
    {
        return Estimate::where('is_accepted', null)->count();
    }

    public function customer() {
        return $this->belongsTo(Customer::class, 'id_customer');
    }


    static public function EstimateToBeOffer() {
        return Estimate::where('is_accepted', true)->count();
    }

}
