<?php

namespace App\Models;

use App\Models\Estimate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'surname',
        'email',
        'contact',
        'company',
        // 'id_estimate',
        'image',
        'body'
    ];

    public function estimates() {
        return $this->hasMany(Estimate::class);
    }

}
