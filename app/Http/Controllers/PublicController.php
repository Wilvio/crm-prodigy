<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Estimate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PublicController extends Controller
{

    public function __construct()
    {
       $customer = Customer::all();
       View::share('customer', $customer);
    }

    
    public function homepage()
    {
        $customer = Customer::all()->sortBy('updated_at', SORT_ASC, 4)->take(5);
        $estimate = Estimate::where('is_accepted', true)->orderBy('updated_at')->take(5)->get();
        return view('welcome', compact('customer','estimate'));
    }
}
