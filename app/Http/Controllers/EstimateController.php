<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Estimate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use phpDocumentor\Reflection\Types\This;

class EstimateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
// funzione per rendere
    public function __construct()
    {
       $customers = Customer::all();
       View::share('customers', $customers);
    }

    public function index()
    {
        $estimates = Estimate::where('is_accepted', null)->orderBy('updated_at')->paginate(8);
        return view('estimate.index', compact('estimates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estimate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estimate = Estimate::create([
            
            'product_name' => $request->product_name,
            'description' => $request->description,
            'price' => $request->price,
            'id_customer' => $request->id_customer,
        ]);
        return redirect (route('estimate.index', compact('estimate')))->with('flash', 'Hai inserito correttamente il Preventivo.');
    }

    // funzioni per accettare o rifiutare un preventivo
    public function setAccepted($estimate_id, $value)
    {
        $estimate = Estimate::find($estimate_id);
        $estimate -> is_accepted = $value;
        $estimate -> save();
        
        if ($value == true) {
            return redirect(route('offer.index'))->with('flash', "OFFERTA creata con successo.");
        } elseif ($value === null) {
            return redirect(route('estimate.index'))->with('flash', "Modifica annullata.");
        } else {
            return redirect(route('estimate.filed'))->with('flash', "Preventivo archiviato con successo.");
        }
        
    }
    public function accepted($estimate_id)
    {
        return $this->setAccepted($estimate_id, true);
    }
    public function declined($estimate_id)
    {
        return $this->setAccepted($estimate_id, false);
    }
// funzione per annullare la modifica
    public function undo($estimate_id)
    {
        return $this->setAccepted($estimate_id, null);
    }
// funzione per visualizzare offerte attive
    public function offer()
    {
        $estimate = Estimate::where('is_accepted', true)->orderBy('updated_at')->paginate(8);
        return view('offer.index', compact('estimate'));
    }
// funzione per archiviare i preventivi
    public function filed()
    {
        $estimate = Estimate::where('is_accepted', false)->orderBy('updated_at')->paginate(8);
        return view('estimate.filed', compact('estimate'));
    }
// funzone per rendere feedback invio offerta
    public function setShipped($estimate_id, $value)
    {
        $estimate = Estimate::find($estimate_id);
        $estimate -> shipped = $value;
        $estimate -> save();
        
        return redirect(route('offer.filed'))->with('flash', "OFFERTA inviata con successo.");
    }
    public function shipped($estimate_id)
    {
        return $this->setShipped($estimate_id, true);
    }
    public function completed()
    {
        $estimate = Estimate::where('shipped', true)->orderBy('updated_at')->paginate(8);
        return view('offer.filed', compact('estimate'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function show(Estimate $estimate)
    {
        $customer = Customer::all();
        return view('estimate.show', compact('estimate', 'customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function edit(Estimate $estimate)
    {
        return view('estimate.edit', compact('estimate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estimate $estimate)
    {
        $estimate->update([
            'product_name' => $request->product_name,
            'description' => $request->description,
            'price' => $request->price,
            'id_customer' => $request->id_customer,
        ]);
        
        return redirect(route('estimate.show', compact('estimate')))->with('flash', 'Preventivo modificato con successo!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Estimate  $estimate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estimate $estimate)
    {
        $estimate->delete();
        return redirect(route('estimate.index'))->with('flash', 'Hai eliminato correttamente il preventivo.');
    }
}
