<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $customer = Customer::create([
            
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'contact' => $request->contact,
            'company' => $request->company,
            'body' => $request->body,
            'image' => $request->file('image')->store('public/image'),
        ]);
        return redirect (route('customer.index'))->with('flash', 'Hai inserito correttamente il Cliente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        return view('customer.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        if($request->image){
            $customer->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'company' => $request->company,
                'body' => $request->body,
                'email' => $request->email,
                'contact' => $request->contact,
                'image' => $request->file('image')->store('puclic/image'),
            ]);
        } else {
            $customer->update([
                'name' => $request->name,
                'surname' => $request->surname,
                'company' => $request->company,
                'body' => $request->body,
                'email' => $request->email,
                'contact' => $request->contact,
            ]);
        };

        return redirect(route('customer.show', compact('customer')))->with('flash', 'Cliente modificato con successo!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect(route('customer.index'))->with('flash', 'Hai eliminato correttamente il cliente.');
    }
}
