Creazione di un CRM per clienti, preventivi e offerte.

Il CRM verrà creato in Laravel, come DB verrà usato MySQL.
Per il front-end bootstrap e Javascript.

Serve una home da cui accedere alle relative pagine:
- Clienti
- Offerte
- Preventivi

La Pagina clienti dovrà avere la lista dei clienti in ordine alfabetico, con la possibilità di filtrarli per offerte o preventivi.
Dovrà contenere tutti i dati disponibili del cliente e la possibilità di contattarlo tramite un form che dovrà mandare una mail all'indirizzo del cliente.

La pagina Offerte dovrà offrire una vista di tutte le offerte in corso, avere una sezione delle offerte accettate e una per quelle rifiutate.
Anche le offerte devono essere filtrabili per cliente e relativo preventivo.

La pagina preventivo sarà come quella delle offerte, filtrabili per cliente e relazionate ad una sola offerta.