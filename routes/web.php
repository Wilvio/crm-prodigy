<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EstimateController;
use App\Http\Controllers\PublicController;
use App\Models\Estimate;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// rotte pubbliche
Route::get('/', [PublicController::class, 'homepage'])->name('home');

// rotte clienti (customer)
Route::get('/customer/index', [CustomerController::class, 'index'])->name('customer.index');
Route::get('/customer/create', [CustomerController::class, 'create'])->name('customer.create');
Route::post('/customer/store', [CustomerController::class, 'store'])->name('customer.store');
Route::get('/customer/show/{customer}', [CustomerController::class, 'show'])->name('customer.show');
Route::get('/customer/edit/{customer}', [CustomerController::class, 'edit'])->name('customer.edit');
Route::put('/customer/update/{customer}', [CustomerController::class, 'update'])->name('customer.update');
Route::delete('/customer/delete/{customer}', [CustomerController::class, 'destroy'])->name('customer.destroy');

// rotte preventivi (estimate)
Route::get('/estimate/index', [EstimateController::class, 'index'])->name('estimate.index');
Route::get('/estimate/create', [EstimateController::class, 'create'])->name('estimate.create');
Route::post('/estimate/store', [EstimateController::class, 'store'])->name('estimate.store');
Route::get('/estimate/show/{estimate}', [EstimateController::class, 'show'])->name('estimate.show');
Route::get('/estimate/edit/{estimate}', [EstimateController::class, 'edit'])->name('estimate.edit');
Route::put('/estimate/update/{estimate}', [EstimateController::class, 'update'])->name('estimate.update');
Route::delete('/estimate/delete/{estimate}', [EstimateController::class, 'destroy'])->name('estimate.destroy');

// rotta preventivi rifiutati -> archivio preventivi
Route::post('/estimate/{id}/declined', [EstimateController::class, 'declined'])->name('estimate.declined');
Route::get('/estimate/filed', [EstimateController::class, 'filed'])->name('estimate.filed');

// rotta per preventivi accettati -> offerte
Route::post('/estimate/{id}/accepted', [EstimateController::class, 'accepted'])->name('estimate.accepted');
Route::get('/offer/index', [EstimateController::class, 'offer'])->name('offer.index');

// rotta per offerte archiviate
Route::get('/offer/filed', [EstimateController::class, 'completed'])->name('offer.filed');

// rotta per rendere preventivo attivo
Route::post('/estimate/{id}/undo', [EstimateController::class, 'undo'])->name('estimate.undo');